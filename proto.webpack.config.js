const autoprefixer = require('autoprefixer')
const path = require('path')

function tryResolve_(url, sourceFilename) {
    try {
        return require.resolve(url, {
            paths: [path.dirname(sourceFilename)]
        })
    } catch (e) {
        return ''
    }
}

function tryResolveScss(url, sourceFilename) {
    const normalizedUrl = url.endsWith('.scss') ? url : `${url}.scss`
    return tryResolve_(normalizedUrl, sourceFilename) ||
        tryResolve_(path.join(path.dirname(normalizedUrl), `_${path.basename(normalizedUrl)}`), sourceFilename)
}

function materialImporter(url, prev) {
    if (url.startsWith('@material')) {
        const resolved = tryResolveScss(url, prev)
        return {
            file: resolved || url
        }
    }
    return {
        file: url
    }
}

module.exports = {
    entry: [
        './src/script.js',
        './src/style.scss'
    ],
    output: {
        path: __dirname + '/dist/',
        filename: 'script.js',
    },
    module: {
        rules: [{
                test: /\.scss$/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'style.css',
                        },
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer()],
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            importer: materialImporter
                        },
                    }
                ],
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015'],
                },
            }
        ],
    },
}