import {
    MDCSelect
} from '@material/select';
import {
    MDCTextField
} from '@material/textfield';

import {
    MDCRipple
} from '@material/ripple'
import {
    parse
} from 'upath';

const $ = require('jquery')
const ElementQueries = require('css-element-queries/src/ElementQueries')

let geoop = function (element) {
    return new geoop.prototype.init(element)
}

geoop.prototype.init = function (element) {
    this.el = element
}

geoop.prototype.load = function (token, options) {
    return new Promise((resolve, reject) => {
        $(() => {

            $.get('geoop/form.html', (res) => {
                let form = $.parseHTML(res)
                for (let i = 1; i <= 12; i++) {
                    $('<li/>', {
                            class: 'mdc-list-item',
                            'data-value': i
                        })
                        .text(i)
                        .appendTo($(form).find('.time-hour__menu>ul'))
                }

                for (let i = 0; i <= 59; i++) {
                    $('<li/>', {
                            class: 'mdc-list-item',
                            'data-value': i
                        })
                        .text(`${i < 10 ? '0' : ''}${i}`)
                        .appendTo($(form).find('.time-minute__menu>ul'))
                }

                $(this.el ? this.el : '#geoop-form-placeholder').html(form)
                resolve(loadElements(this.el ? this.el : '#geoop-form-placeholder'))
            })
        })
    })
}
geoop.prototype.init.prototype = geoop.prototype

geoop.load = (token, options) => {
    geoop().load(token, options)
}

let loadElements = (el) => {
    const request_type = new MDCSelect($(el).find('#request-type')[0])
    const request_details = new MDCTextField($(el).find('#request-details')[0])
    const first_name = new MDCTextField($(el).find('#first-name')[0])
    const last_name = new MDCTextField($(el).find('#last-name')[0])
    const phone = new MDCTextField($(el).find('#phone')[0])
    const email = new MDCTextField($(el).find('#email')[0])
    const address = new MDCTextField($(el).find('#address')[0])
    const address2 = new MDCTextField($(el).find('#address2')[0])
    const city = new MDCTextField($(el).find('#city')[0])
    const state = new MDCTextField($(el).find('#state')[0])
    const postcode = new MDCTextField($(el).find('#postcode')[0])
    const preferred_time = new MDCTextField($(el).find('#preferred-time')[0])
    const time_hour = new MDCSelect($(el).find('#time-hour')[0])
    const time_minute = new MDCSelect($(el).find('#time-minute')[0])
    const meridiem = new MDCSelect($(el).find('#meridiem')[0])
    MDCRipple.attachTo($(el).find('#submit')[0])

    ElementQueries.init()

    $(el).find('#geoop-form').submit((e) => {
        e.preventDefault()
        let data = {}

        $(e.target).serializeArray().forEach((el) => {
            data[el.name] = el.value
        })

        alert(JSON.stringify(data))
    })

    $(el).find('input').keyup((e) => {
        if ($(e.target).val()) $(e.target).parent().removeClass('mdc-text-field--invalid')
    })


    time_hour.listen('MDCSelect:change', () => updatePreferredTime('hour', time_hour.value))
    time_minute.listen('MDCSelect:change', () => updatePreferredTime('minute'), time_minute.value)
    meridiem.listen('MDCSelect:change', () => updatePreferredTime('meridiem'), meridiem.value)

    function updatePreferredTime(time_el, val) {
        console.log(val)
        let time__input = $(el).find('#preferred-time').find('input')
        let cur_val = time__input.val()
        let new_val

        let cur_hr, cur_min

        if (time_el != 'meridiem') {
            cur_hr = cur_val.split(':')[0]
            cur_min = cur_val.split(':')[1]
            val = parseInt(val)
        }

        if (time_el == 'hour') {
            val = (val == 12 && parseInt(cur_hr) <= 12 ? 0 : 12) ? val : 0
            let new_hr = val + (parseInt(cur_hr) <= 12 ? 0 : 12)
            new_val = `${new_hr<10 ? '0' : ''}${new_hr}:${cur_min}`
        } else if (time_el == 'minute') {
            console.log(val)
            new_val = `${cur_hr}:${(val<10 ? '0' : '')+val}`
        } else {
            let cur_hr =
                parseInt(cur_val.split(':')[0]) < 12 ?
                parseInt(cur_val.split(':')[0]) :
                parseInt(cur_val.split(':')[0]) - 12
            new_val = `${cur_hr + (val == 'PM' ? 12 : 0)}:${cur_val.split(':')[1]}`
        }
        console.log(new_val)
        time__input.val(new_val)
    }

    return $(el).find('#geoop-form')[0]
}

window.geoop = geoop