import {
    MDCSlider
} from '@material/slider';
import {
    MDCDialog
} from '@material/dialog';

const $ = require('jquery')

$(() => {
    geoop($('#form-wrapper')[0]).load().then((form) => {
        $('.mdc-dialog__title>.title').prepend($('#form-title').text())
        $('#form-title').hide()
        $('#submit').remove()
        $('#modal-submit').click(() => {
            let data = {}
            $(form).serializeArray().forEach((el) => {
                data[el.name] = el.value
            })
            alert(JSON.stringify(data))
        })
    })

    const slider = new MDCSlider($('.mdc-slider')[0])
    slider.listen('MDCSlider:input', () => {
        $('#geoop-form-placeholder').width(slider.value + '%')
    })
    const modal = new MDCDialog($('#modal')[0])


    $('#modal-toggle').click(() => modal.open())
})